from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from expenditures.views import ExpenditureViewSet, ExpenditureCategoryViewSet, ExpenditureItemViewSet

router = routers.SimpleRouter()
router.register(r'expenditure', ExpenditureViewSet)
router.register(r'expenditure_item', ExpenditureItemViewSet)
router.register(r'expenditure_category', ExpenditureCategoryViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]
