from django.db.models import aggregates, functions, AutoField
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK

from expenditures.models import Expenditure, ExpenditureItem, ExpenditureCategory
from expenditures.serializers import ExpenditureSerializer, ExpenditureCategorySerializer, ExpenditureItemSerializer, \
    ChartSerializer

trunc_by_dict = {'year': functions.TruncYear,
                 'quarter': functions.TruncQuarter,
                 'month': functions.TruncMonth,
                 'week': functions.TruncWeek,
                 'day': functions.TruncDay,
                 'overall': None}


def parse_csv_values(values_str):

    if values_str is not None:
        return values_str.split(',')

    return None

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class AggregationObject(object):
    aggregate_methods = {'sum': aggregates.Sum, 'avg': aggregates.Avg}

    @classmethod
    def get_annotate_dict(cls, method_key, columns):
        d = {}
        if columns is not None:
            d = {col + '_' + method_key: cls.aggregate_methods[method_key](col) for col in columns}

        return d

    @classmethod
    def get_annotate_list(cls, method_key, columns):
        result = []
        if columns is not None:
            result = [val + '_' + method_key for val in columns]

        return result


class ExpenditureViewSet(viewsets.ModelViewSet):
    queryset = Expenditure.objects.all()
    serializer_class = ExpenditureSerializer
    pagination_class = StandardResultsSetPagination
    # permission_classes = [IsAdminUser]

    def get_queryset(self, **kwargs):
        if 'page_size' not in self.request.query_params:
            self.pagination_class = None

        if kwargs.get('chart_x'):
            date_processor = self.request.GET.get('date_processor', 'overall')
            query = self.process_date(self.queryset, date_processor)
            group_by_keys = self.request.query_params.get('group_by')
            annotate = self.get_annotate_dict()
            query = self.group_by(query, group_by_keys, annotate, date_processor)
            print(query)

            return query

        return super().get_queryset()

    def process_date(self, queryset, date_processor):
        trunc_by = trunc_by_dict[date_processor]

        if trunc_by is not None:
            queryset = queryset.annotate(time_span=trunc_by('date'))
        return queryset

    def get_annotate_dict(self):
        annotate = {}
        for method in AggregationObject.aggregate_methods.keys():
            aggregation_columns = parse_csv_values(self.request.query_params.get(method))

            if aggregation_columns:
                aggregate = AggregationObject.get_annotate_dict(method, aggregation_columns)
                annotate.update(aggregate)

        return annotate

    def group_by(self, query, group_by_keys, annotate, date_processor):
        group_by_keys = parse_csv_values(group_by_keys) or []
        if date_processor:
            group_by_keys += ['time_span']

        if len(group_by_keys) == 0:
            group_by_keys = self.default_fields()

        return query.values(*group_by_keys).annotate(**annotate)

    def get_aggregate_serialize_fields(self):
        aggregate_fields = []
        for method in AggregationObject.aggregate_methods.keys():
            aggregate_values = parse_csv_values(self.request.query_params.get(method))

            if aggregate_values:
                aggregate = AggregationObject.get_annotate_list(method, aggregate_values)
                aggregate_fields.extend(aggregate)

        return aggregate_fields

    def get_serializer(self, *args, **kwargs):
        # select_param = self.request.GET.get('select')
        # kwargs['select_only_as'] = parse_select_only_as(select_param)
        if self.request.query_params.get('chart_x'):
            aggregate_fields = self.get_aggregate_serialize_fields()
            serialize_fields = self.request.query_params.get('group_by')
            date_processor = self.request.query_params.get('date_processor')
            kwargs['serialize_fields'] = self.get_serializer_fields(serialize_fields, aggregate_fields, date_processor)

        return self.serializer_class(*args, **kwargs)

    def default_fields(self):
        return [k.name for k in Expenditure._meta.fields if not isinstance(k, AutoField)]

    def get_serializer_fields(self, serialize_fields, aggregate_fields, date_processor):
        fields = parse_csv_values(serialize_fields) or self.default_fields()

        if len(aggregate_fields) > 0:
            fields.extend(aggregate_fields)

        if date_processor:
            fields += ['time_span']

        return fields

    @action(methods=['get'], detail=False)
    def chart_x(self, request):
        kwargs = {
            'chart_x': self.request.GET.get('chart_x')
        }
        queryset = self.filter_queryset(self.get_queryset(**kwargs))
        serializer = ChartSerializer(queryset, many=True, **kwargs)

        # date_business_gte = self.request.query_params.get('date_business_gte')
        # date_business_lte = self.request.query_params.get('date_business_lte')
        # date_processor = self.request.query_params.get('date_processor')
        #
        # if not all([date_business_gte, date_business_lte, date_processor]):
        #     return Response(status=HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status=HTTP_200_OK)


class ExpenditureItemViewSet(viewsets.ModelViewSet):
    queryset = ExpenditureItem.objects.all()
    serializer_class = ExpenditureItemSerializer


class ExpenditureCategoryViewSet(viewsets.ModelViewSet):
    queryset = ExpenditureCategory.objects.all()
    serializer_class = ExpenditureCategorySerializer
