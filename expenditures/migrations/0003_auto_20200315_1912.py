# Generated by Django 3.0.4 on 2020-03-15 18:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('expenditures', '0002_auto_20200315_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='expenditureitem',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='expenditures.ExpenditureCategory'),
        ),
    ]
