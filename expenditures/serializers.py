from collections import defaultdict

from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.serializers import LIST_SERIALIZER_KWARGS, ListSerializer
from rest_framework.utils.serializer_helpers import ReturnDict

from expenditures.models import Expenditure, ExpenditureItem, ExpenditureCategory


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class ExpenditureCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpenditureCategory
        fields = '__all__'


class ExpenditureItemSerializer(serializers.ModelSerializer):
    category = ExpenditureCategorySerializer()

    class Meta:
        model = ExpenditureItem
        fields = '__all__'


class ExpenditureSerializer(serializers.ModelSerializer):
    item = ExpenditureItemSerializer()

    class Meta:
        model = Expenditure
        fields = '__all__'


class ChartXSerializer(serializers.ListSerializer):

    def __init__(self, *args, **kwargs):
        # self.serialize_fields = kwargs.pop('serialize_fields', None)
        # self.select_only_as = kwargs.pop('select_only_as', {})
        self.chart_x = kwargs.pop('chart_x', None)
        super().__init__(*args, **kwargs)

    @property
    def data(self):
        if self.chart_x:
            ret = self.org_data
            return ReturnDict(ret, serializer=self)

        return super().data

    @property
    def org_data(self):
        if hasattr(self, 'initial_data') and not hasattr(self, '_validated_data'):
            msg = (
                'When a serializer is passed a `data` keyword argument you '
                'must call `.is_valid()` before attempting to access the '
                'serialized `.data` representation.\n'
                'You should either call `.is_valid()` first, '
                'or access `.initial_data` instead.'
            )
            raise AssertionError(msg)

        if not hasattr(self, '_data'):
            if self.instance is not None and not getattr(self, '_errors', None):
                self._data = self.to_representation(self.instance)
            elif hasattr(self, '_validated_data') and not getattr(self, '_errors', None):
                self._data = self.to_representation(self.validated_data)
            else:
                self._data = self.get_initial()
        return self._data

    def parse_values_keys(self, values_keys):
        """
        Return a dict of mapped values keys requested from query param including 'select' param.
        :type values_keys: List[string]
        """
        # if self.select_only_as:
        #     dimensions_keys = list(filter(lambda x: x in self.select_only_as.keys(), dimensions_keys))
        values_keys = list(filter(lambda x: x not in self.chart_x, values_keys))

        return values_keys

    def to_representation(self, data):
        print(data)
        if self.chart_x:
            chart_x_values = [item.get(self.chart_x) for item in data]
            values = defaultdict(list)
            values_keys = []
            if data and len(data) > 0:
                values_keys = data[0].keys()
                values_keys = self.parse_values_keys(values_keys)

            for key in values_keys:
                for item in data:
                    values[key].append(item.get(key))

            return {'chart_x': chart_x_values,
                    'values': values}
        return data


class ChartSerializer(serializers.Serializer):
    chart_x_key = "chart_x"
    LIST_SERIALIZER_KWARGS_LOCAL = LIST_SERIALIZER_KWARGS + ('chart_x',)

    class Meta:
        list_serializer_class = ChartXSerializer

    def __init__(self, *args, **kwargs):
        self.serializer_fields = kwargs.pop('serializer_fields', [])
        self.chart_x = kwargs.pop(self.chart_x_key)

        for field in self.serializer_fields:
            field_method_name = 'get_' + field
            m = self.get_method(field)
            setattr(self, field_method_name, m)
            setattr(self, field, serializers.SerializerMethodField())

        if kwargs.get('many') and self.chart_x:
            kwargs[self.chart_x_key] = self.chart_x

        super().__init__(self, *args, **kwargs)

    @classmethod
    def many_init(cls, *args, **kwargs):
        """
        This method implements the creation of a `ListSerializer` parent
        class when `many=True` is used. You can customize it if you need to
        control which keyword arguments are passed to the parent, and
        which are passed to the child.

        Note that we're over-cautious in passing most arguments to both parent
        and child classes in order to try to cover the general case. If you're
        overriding this method you'll probably want something much simpler, eg:

        @classmethod
        def many_init(cls, *args, **kwargs):
            kwargs['child'] = cls()
            return CustomListSerializer(*args, **kwargs)
        """
        allow_empty = kwargs.pop('allow_empty', None)
        child_serializer = cls(*args, **kwargs)
        list_kwargs = {
            'child': child_serializer,
        }
        if allow_empty is not None:
            list_kwargs['allow_empty'] = allow_empty
        list_kwargs.update({
            key: value for key, value in kwargs.items()
            if key in cls.LIST_SERIALIZER_KWARGS_LOCAL
        })
        meta = getattr(cls, 'Meta', None)
        list_serializer_class = getattr(meta, 'list_serializer_class', serializers.ListSerializer)
        return list_serializer_class(*args, **list_kwargs)

    def get_method(self, field):
        def inner(obj):
            return obj.get(field)
        return inner

    def get_fields(self):
        return {k: getattr(self, k) for k in self.serializer_fields}
