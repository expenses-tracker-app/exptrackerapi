from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class AbstractItem(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class ExpenditureCategory(AbstractItem):

    class Meta:
        verbose_name = "Expenditure category"
        verbose_name_plural = "Expenditure categories"


class ExpenditureItem(AbstractItem):
    category = models.ForeignKey(ExpenditureCategory, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Expenditure item"
        verbose_name_plural = "Expenditure items"


class Expenditure(models.Model):
    item = models.ForeignKey(ExpenditureItem, on_delete=models.PROTECT)
    price = models.DecimalField(decimal_places=2, max_digits=7)
    qty = models.IntegerField()
    date = models.DateField(default=timezone.now)
    payer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="payed_expenditures")
    owner = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name="owned_expenditures")

    class Meta:
        verbose_name = "Expenditure"
        verbose_name_plural = "Expenditures"

    def __str__(self):
        return self.item.name
