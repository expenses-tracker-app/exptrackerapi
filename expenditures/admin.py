from django.contrib import admin
from django.contrib.admin import register

from expenditures.models import Expenditure, ExpenditureItem, ExpenditureCategory


@register(Expenditure)
class ExpenditureAdmin(admin.ModelAdmin):
    pass


@register(ExpenditureCategory)
class ExpenditureCategoryAdmin(admin.ModelAdmin):
    pass


@register(ExpenditureItem)
class ExpenditureAdmin(admin.ModelAdmin):
    pass
